#include<stdio.h>
#include<string.h>
#include<math.h>
#include<string.h>

int main()
{
    int maxPowerPos,update,numSpider,isSkip,updatedIndex;
    int dequeueCount,reqSpider,updatedPower[1000],retainPos[1000],maxPower,spiderPos[100010],powers[100010],index,spiderIndex;
    scanf("%d%d",&numSpider,&reqSpider);
    for(index=1;index<=numSpider;index++)
    {
        scanf("%d",&powers[index]);
        spiderPos[index]=index;
    }
    for(spiderIndex=1;spiderIndex<=reqSpider;spiderIndex++)
    {
        maxPower=-1;
        if(numSpider<reqSpider)
            dequeueCount=numSpider;
        else
            dequeueCount=reqSpider;

        //finding max power
        for(index=1;index<=dequeueCount;index++)
            if(powers[index]>maxPower)
            {
                maxPower=powers[index];
                maxPowerPos=spiderPos[index];
            }

        isSkip=0;
        //updating the power
        for(index=1;index<=reqSpider;index++)
        {
            if(maxPowerPos==spiderPos[index])
            {
                isSkip=1;
                continue;
            }
            if(powers[index]>0)
                updatedPower[index-isSkip]=powers[index]-1;
            else
                updatedPower[index-isSkip]=powers[index];
            retainPos[index-isSkip]=spiderPos[index];
        }

        //shifting the remaining spiders
        for(index=1;index<=numSpider-reqSpider;index++)
        {
            powers[index]=powers[index+reqSpider];
            spiderPos[index]=spiderPos[index+reqSpider];
        }

        updatedIndex=1;
        //enqueueing
        for(;index<numSpider;index++)
        {
            powers[index]=updatedPower[updatedIndex];
            spiderPos[index]=retainPos[updatedIndex++];
        }
        numSpider--;
        printf("%d ",maxPowerPos);
    }
    printf("\n");
    return 0;
}